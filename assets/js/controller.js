
let allCompanies = [];

// Controller functions here...

function loadCompanies() {
    fetchCompanies().then(
        function (companies) {
            allCompanies = companies;
            displayCompanies(allCompanies);
        }
    );
}

function displayCompanies(companies) {
    let companyHtml = "";

    for(let i = 0; i < companies.length; i++) {
        companyHtml = companyHtml + `
            <tr>
                <td>
                    ${companies[i].id}
                </td>
                <td>
                    ${companies[i].name}
                </td>
                <td>
                    <img src="${companies[i].logo}" height="40">
                </td>
                <td>
                    <button 
                        class="btn btn-danger" 
                        onclick="handleDeleteButtonClick(${companies[i].id})">
                        Kustuta
                    </button>
                    <button 
                        class="btn btn-primary" 
                        onclick="handleEditButtonClick(${companies[i].id})">
                        Muuda
                    </button>
                </td>
            </tr>
        `;
    }
    document.getElementById("companiesList").innerHTML = companyHtml;
}

function handleSearchkeyChange() {
    let searchKey = document.getElementById("searchKey").value;
    let filteredCompanies = searchCompanies(searchKey);
    displayCompanies(filteredCompanies);
}

function searchCompanies(searchKeyword) {
    let resultingCompanies = [];

    for (let i = 0; i < allCompanies.length; i++) {
        if (allCompanies[i].name.toLowerCase().search(searchKeyword.toLowerCase()) != -1) {
            resultingCompanies.push(highlightSearchMatch(allCompanies[i], searchKeyword));
        }
    }

    return resultingCompanies;
}

function highlightSearchMatch(company, searchKey) {
    let highlightedCompany = {...company}; // Teeme koopia algsest ettevõttest, ära muuda originaali!!!
    const re = new RegExp(searchKey, "gi");
    highlightedCompany.name = highlightedCompany.name.replace(re, `<span style="color: green; font-weight: bolder; background: grey;">${searchKey}</span>`);
    return highlightedCompany;
}

function handleDeleteButtonClick(id) {
    if (confirm("Oled sa ikka kindel, et soovid seda ettevõtet kustutada?")) {
        deleteCompany(id).then(loadCompanies);
    }
}

function handleEditButtonClick(id) {
    $("#companyModal").modal("show");
    fetchCompany(id).then(
        function(company) {
            document.getElementById("id").value = company.id;
            document.getElementById("name").value = company.name;
            document.getElementById("logo").value = company.logo;
        }
    )
}

function handleSave() {
    if (isFormValid() === false) {
        return;
    }
    if (document.getElementById("id").value > 0) {
        // Edit
        handleEdit();
    } else {
        // Add
        handleAdd();
    }
}

function handleEdit() {
    let company = {
        id: document.getElementById("id").value,
        name: document.getElementById("name").value,
        logo: document.getElementById("logo").value
    };
    putCompany(company).then(
        function() {
            loadCompanies();
            $("#companyModal").modal("hide");
        }
    );
}

function handleAddButtonClick() {
    $("#companyModal").modal("show");
    document.getElementById("id").value = null;
    document.getElementById("name").value = null;
    document.getElementById("logo").value = null;
}

function handleAdd() {
    let company = {
        name: document.getElementById("name").value,
        logo: document.getElementById("logo").value
    };
    postCompany(company).then(
        function() {
            loadCompanies();
            $("#companyModal").modal("hide");
        }
    );
}

// Validation
function isFormValid() {
    let name = document.getElementById("name").value;
    let logo = document.getElementById("logo").value;

    if (name === null || name.length < 1) {
        document.getElementById("errorMessage").innerText = "Ettevõtte nimi on kohustuslik!";
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if (logo === null || logo.length < 1) {
        document.getElementById("errorMessage").innerText = "Ettevõtte logo on kohustuslik!";
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    document.getElementById("errorMessage").style.display = "none";
    return true;
}
